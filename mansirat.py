def check_word(word, final_word, dic):
    if word == final_word:
        return True
    
    for i in range(len(word)):
        new_word = word[:i] + final_word[i] + word[i+1:]
        
        if new_word == final_word:
            c+=1
            print("Next:", new_word)
            return True
        
        if new_word in dic:
            c+=1
            print("Next:", new_word)
            dic.remove(new_word)  
            if check_word(new_word, final_word, dic):
                return True
            dic.append(new_word) 
    
    return False

dic = ['clay', 'clad', 'glad', 'gold', 'goad']
word = 'clay'
final_word = 'gold'

if check_word(word, final_word, dic):
    print("steps:",c)
else:
    print("No  path found.")
